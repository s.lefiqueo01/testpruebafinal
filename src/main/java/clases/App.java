package clases;

import model.Network;

import java.util.logging.Logger;

public class  App {

    public static void main(String[] args) {

        Wine wineTest = new Wine();
        Network<Integer>.Results results = wineTest.classify();
        string=results.correct + " correct of " + results.trials + " = " +
                results.percentage * 100 + "%";
        logger.info(string);
    }

    private static String string;
    private static final Logger logger = Logger.getLogger(string);
}
